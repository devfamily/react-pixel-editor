import { 
    CHANGE_TOOL, CELL_DRAWING, CHANGE_COLOR, 
    SET_BOARD_COPY, 
    DRAW_PRIMITIVE, CANCEL_DRAW_PRIMITIVE, CELL_ARRAY_DRAWING,
} from "./actions";
import { 
    DRAWING_TOOLS, PENCIL, 
    ERASER, RECTANGLE, LINE, 
    CIRCLE, FILL, TRANSPARENT} from "./modules/toolsConstants";
import { HEIGHT, WIDTH } from "./editorConstants";
import { 
    getRectCoordinates, 
    getLineCoordinates,
    getEllipseCoordinates
} from "./modules/lib/calcCoordsPrimitives";
import { getFillArea } from "./modules/lib/calcFillingCoords";

const getBoardCopy = (board) => {
    let boardCopy = (new Array(HEIGHT)).fill(TRANSPARENT);
    boardCopy = board.map((row) => (row.slice()));
    return boardCopy
}


export default (state = init(), actions) => {
    switch (actions.type) {
        case CHANGE_TOOL:
            return changeTool(state, actions.tool);
        case CELL_DRAWING:
            return cellDrawing(state, actions.pointCoord);
        case CHANGE_COLOR:
            return changeColor(state, actions.color);
        case DRAW_PRIMITIVE:
            return drawPrimitive(state, actions.start, actions.end);
        case SET_BOARD_COPY:
            return setBoardCopy(state);
        case CANCEL_DRAW_PRIMITIVE:
            return cancelDrawPrimitive(state);
        case CELL_ARRAY_DRAWING:
            return cellArrayDrawing(state, actions.start, actions.end);
        default:
            return state;
    }
}


const init = () => {
    let board = (new Array(HEIGHT)).fill(null);
    board = board.map(() => (new Array(WIDTH)).fill(TRANSPARENT)) ;
    return {
        tool: PENCIL,
        color: 'black',
        board
    }
}


const changeTool = (state, tool) => {
    if (!Object.keys(DRAWING_TOOLS).includes(tool)) {
        return state;
    }

    return {
        ...state,
        tool
    }
}


const cellDrawing = (state, pointCoord) => {
    const [y, x] = pointCoord;
    let board = getBoardCopy(state.board);
    switch (state.tool){
        case PENCIL:
            board[y][x] = state.color; 
            break;
        case ERASER:
            board[y][x] = TRANSPARENT; 
            break;
        case FILL:
            for (let [dy, dx]  of getFillArea(board, pointCoord)){
                board[dy][dx] = state.color;
            }
            break;
        default:
            return state
    }
    
    return {
        ...state,
        board
    }
}


const changeColor = (state, color) => {
    return {
        ...state,
        color
    }
}


const setBoardCopy = (state) => {
    return {
        ...state,
        boardCopy: getBoardCopy(state.board)
    }
}


const drawPrimitive = (state, start, end) => {
    const [startY, startX] = start;
    const [endY, endX] = end;

    let board = getBoardCopy(state.boardCopy);

    if (state.tool === RECTANGLE){
        const [dy, dx] = getRectCoordinates(startY, startX, endY, endX);

        for(let x of dx){
            board[startY][x] = state.color;
            board[endY][x] = state.color;
        }

        for(let y of dy){
            board[y][startX] = state.color
            board[y][endX] = state.color
        }
    } else if (state.tool === LINE) {
        let coords = getLineCoordinates(startY, startX, endY, endX);
        for (let [dy, dx] of coords){
            board[dy][dx] = state.color
        }
    } else if (state.tool === CIRCLE) {
        let coords = getEllipseCoordinates(startY, startX, endY, endX);
        for (let [dy, dx] of coords){
            board[dy][dx] = state.color
        }
    }

    return {
        ...state,
        board
    }
}


const cancelDrawPrimitive = (state) => {
    return {
        ...state,
        board: getBoardCopy(state.boardCopy),
        boardCopy: []
    }
}


const cellArrayDrawing = (state, start, end) => {
    const [startY, startX] = start;
    const [endY, endX] = end;

    let board = getBoardCopy(state.board);
    let coords = getLineCoordinates(startY, startX, endY, endX);
        for (let [dy, dx] of coords){
            board[dy][dx] = state.tool === PENCIL ? state.color: TRANSPARENT;
        }
    
    return{
        ...state,
        board
    }
}