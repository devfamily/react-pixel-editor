import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import { HEIGHT, WIDTH, CELL_WIDTH, LEFT_MOUSE_BUTTON, PREVIEW_WIDTH, PREVIEW_HEIGHT, CANVAS_WIDTH, CANVAS_HEIGHT } from '../editorConstants';
import { PENCIL, ERASER, RECTANGLE, LINE, CIRCLE, TRANSPARENT } from './toolsConstants';

class Canvas extends Component {
    constructor(props) {
        super(props);
        this.startPoint = [];
        this.endPoint = [];
    }


    render() {
        const coverStyle = {
            width: `${CANVAS_WIDTH}px`,
            height: `${CANVAS_HEIGHT}px`,
        }

        const canvasStyle = {
            backgroundSize: `${CELL_WIDTH}px ${CELL_WIDTH}px`
        }
        return (
            <div className="cover" style={coverStyle}>
                <canvas ref="canvas" id="canvas" 
                    style={canvasStyle} width={CANVAS_WIDTH} height={CANVAS_HEIGHT}

                    onMouseUp={(event) => this.handleMouseUp(event)}
                    onMouseLeave={() => this.handleMouseLeave()}
                    onMouseMove={(event) => this.handleMouseMove(event)}
                    onClick={(event) => this.handleClick(event)}>
                </canvas>
            </div>
        )
    }


    componentDidMount() {
        this.updateCanvas();
    }


    componentDidUpdate() {
        this.updateCanvas();
    }


    updateCanvas() {
        const ctx = this.refs.canvas.getContext('2d');
        
        ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        
        const { state } = this.props;
        for (let dy = 0; dy < HEIGHT; dy++) {
            for (let dx = 0; dx < WIDTH; dx++) {
                if(state.board[dy][dx] !== TRANSPARENT){
                    ctx.fillStyle = state.board[dy][dx];
                    ctx.fillRect(dx * CELL_WIDTH, dy * CELL_WIDTH, CELL_WIDTH, CELL_WIDTH);
                }
            }
        }

        const previewCtx = document.getElementById('preview').getContext('2d');
        previewCtx.clearRect(0, 0, PREVIEW_WIDTH, PREVIEW_HEIGHT);
        previewCtx.drawImage(this.refs.canvas, 0, 0, PREVIEW_WIDTH, PREVIEW_HEIGHT);
    }


    getPixelCoords(clientX, clientY){
        const coords = this.refs.canvas.getBoundingClientRect();
        return [
            Math.ceil((clientY - coords.top) / CELL_WIDTH) - 1,
            Math.ceil((clientX - coords.left) / CELL_WIDTH) - 1
        ]
    }


    isCoordsEqual(coord1, coord2){
        return (coord1.length > 0 && coord2.length > 0 && 
                    coord1[0] === coord2[0] && coord1[1] === coord2[1])
    }


    isInsideBoard(point){
        return point[0] >=0 && point[0] < HEIGHT && 
                point[1] >=0 && point[1] < WIDTH
    }


    handleClick(event){
        const pointCoord = this.getPixelCoords(event.clientX, event.clientY);
        const { cellDrawing } = this.props;
        cellDrawing(pointCoord)
    }


    handleMouseMove(event) {
        if (event.buttons === LEFT_MOUSE_BUTTON) {
            const { state } = this.props;
            const targetCoords = this.getPixelCoords(event.clientX, event.clientY);
            if (!this.isInsideBoard(targetCoords)){
                return ;
            }

            if (this.startPoint.length === 0){
                this.startPoint = [
                    targetCoords[0],
                    targetCoords[1] 
                ];

                if ([RECTANGLE, LINE, CIRCLE].includes(state.tool)) {
                    const { setBoardCopy } = this.props;
                    setBoardCopy();
                }
            }

            if ([PENCIL, ERASER].includes(state.tool)) {
                if (!this.isCoordsEqual(this.startPoint, targetCoords)) {
                    const { cellArrayDrawing } = this.props;
                    cellArrayDrawing(this.startPoint, targetCoords)
                    this.startPoint = [
                        targetCoords[0],
                        targetCoords[1]
                    ]
                }
            } else if ([RECTANGLE, LINE, CIRCLE].includes(state.tool)) {
                if (!this.isCoordsEqual(this.startPoint, targetCoords) &&
                    !this.isCoordsEqual(this.endPoint, targetCoords)) {

                    this.endPoint = [
                        targetCoords[0],
                        targetCoords[1]
                    ]
                    const { drawPrimitive } = this.props;
                    drawPrimitive(this.startPoint, this.endPoint);
                }
            }
        }
    }


    handleMouseLeave() {
        this.startPoint = [];
        this.endPoint = [];
    }


    handleMouseUp(event) {
        const targetCoord = this.getPixelCoords(event.clientX, event.clientY);
        if ([RECTANGLE, LINE, CIRCLE].includes(this.props.state.tool) && 
                this.isCoordsEqual(this.startPoint, targetCoord)) {

            const { cancelDrawPrimitive } = this.props;
            cancelDrawPrimitive();
        }
        
        this.startPoint = [];
        this.endPoint = [];
    }
}

export default connect((state) => ({ state }), actions)(Canvas);