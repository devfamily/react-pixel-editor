import React, {Component} from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';
import { DRAWING_TOOLS } from './toolsConstants';

class Tool extends Component{
    render(){
        const { property, state } = this.props;
        const activeStyle = state.tool === property
                            ? "active"
                            : "";

        return(
            <div className={ "tool-btn " + activeStyle } onClick={() => this.handleClick(property)}>
                <i className={ DRAWING_TOOLS[property] }></i>
            </div>
        )
    }


    handleClick(tool){
        const {changeTool} = this.props;
        changeTool(tool);
    }
}

export default connect((state) => ({ state }), actions)(Tool);