import { HEIGHT, WIDTH } from "../../editorConstants";

export const getLineCoordinates = (startY, startX, endY, endX) => {
    // вертикальная черта
    if (startY === endY) {
        return filterCanvasBorder(
                    getCoordsOffset(startX, endX).map((item) => ([startY, item])))
    }

    // горизонтальная черта
    if (startX === endX) {
        return filterCanvasBorder(
                    getCoordsOffset(startY, endY).map((item) => ([item, startX])))
    }

    // наклонная
    if (Math.abs(startX - endX) > Math.abs(startY - endY)) {
        const angularRatio = Math.abs((endY - startY) / (endX - startX));
        const xCoords = getCoordsOffset(startX, endX);
        const yCoords = bresenhemLine(startY, endY, angularRatio, xCoords.length);

        return filterCanvasBorder(
                    yCoords.map((item, inx) => ([item, xCoords[inx]])))
    } else {
        const angularRatio = Math.abs((endX - startX) / (endY - startY));
        const yCoords = getCoordsOffset(startY, endY);
        const xCoords = bresenhemLine(startX, endX, angularRatio, yCoords.length);

        return filterCanvasBorder(
                    yCoords.map((item, inx) => ([item, xCoords[inx]])))
    }
}


export const getRectCoordinates = (startY, startX, endY, endX) => {
    let xCoords = getCoordsOffset(startX, endX);
    let yCoords = getCoordsOffset(startY, endY);

    return [yCoords, xCoords]
}


export const getEllipseCoordinates = (startY, startX, endY, endX) => {
    const radius = Math.min(
        Math.floor(Math.abs(startX - endX) / 2),
        Math.floor(Math.abs(startY - endY) / 2)
    );
    
    let centerTopY, centerTopX, centerBottomY, centerBottomX;

    // учитываем разные четверти осей координая
    if (startY < endY){
        centerTopY = startY + radius;
        centerBottomY = endY - radius;
    } else {
        centerTopY = endY + radius;
        centerBottomY = startY - radius;
    }

    if (startX < endX) {
        centerTopX = startX + radius;
        centerBottomX = endX - radius;
    } else {
        centerTopX = endX + radius;
        centerBottomX = startX - radius;
    }

    let [y, x] = [0, radius];
    let [dy, dx] = [1, 1]; 
    let err = dx - radius * 2;
    let coords = [];

    while(x >= y){
        coords.push([centerBottomY + x, centerTopX - y]);
        coords.push([centerBottomY + y, centerTopX - x]);
        coords.push([centerTopY - y, centerTopX - x]);
        coords.push([centerTopY - x, centerTopX - y]);

        if (Math.abs(startY - endY) > Math.abs(startX - endX)){
            coords.push([centerBottomY + y, centerTopX + x]);
            coords.push([centerBottomY + x, centerTopX + y]);
            coords.push([centerTopY - x, centerTopX + y]);
            coords.push([centerTopY - y, centerTopX + x]);
        } else {
            coords.push([centerBottomY + y, centerBottomX + x]);
            coords.push([centerBottomY + x, centerBottomX + y]);
            coords.push([centerTopY - x, centerBottomX + y]);
            coords.push([centerTopY - y, centerBottomX + x]);
        }

        if(err <= 0){
            y++;
            err += dy;
            dy +=2;
        } 

        if(err > 0) {
            x--;
            dx +=2;
            err += dx - radius * 2
        }
    }

    if (centerBottomX - centerTopX > 1){
    const addCoords = getCoordsOffset(centerTopX, centerBottomX);
    coords = coords.concat(
                addCoords.map((item) => ([centerTopY - radius, item]))
            ).concat(
                addCoords.map((item) => ([centerBottomY + radius, item])) 
            )
        }

    if (centerBottomY - centerTopY > 1){
        const addCoords = getCoordsOffset(centerTopY, centerBottomY);
        coords = coords.concat(
                    addCoords.map((item) => ([item, centerTopX - radius]))
                ).concat(
                    addCoords.map((item) => ([item, centerTopX + radius ]))
                )
    }
    return filterCanvasBorder(coords)
}


const filterCanvasBorder = (coords) => {
    return coords.filter(item => (
        item[0] >= 0 && item[0] < HEIGHT && item[1] >= 0 && item[1] < WIDTH
    ))
}

const getCoordsOffset = (start, end) => {
    const step = start < end ? 1 : -1;
    return Array(Math.abs(start - end) + 1)
        .fill()
        .map((_, inx) => (start + inx * step))
        .filter(item => (item >= 0));
}


const bresenhemLine = (start, end, angularRatio, len) => {
    let err = 0;
    let coord = start;
    return [start].concat(
        Array(len - 1)
            .fill()
            .map((_) => {
                err += angularRatio;
                if (err >= 0.5) {
                    coord += (end - start) > 0 && (end - start) !== 0 ? 1 : -1;
                    err -= 1;
                }
                return coord
            })
    )
}