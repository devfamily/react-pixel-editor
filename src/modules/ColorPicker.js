import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SketchPicker } from 'react-color';

import * as actions from '../actions';

class ColorPicker extends Component {
    render() {
        return (
            <div>
                <SketchPicker
                    color={this.props.state.color} width='120px'
                    disableAlpha={true}
                    onChangeComplete={(color) => this.props.changeColor(color.hex)}
                />
            </div>
        )
    }

    shouldComponentUpdate(nextProps){
        return this.props.state.color !== nextProps.state.color
    }
}

export default connect((state) => ({ state }), actions)(ColorPicker);