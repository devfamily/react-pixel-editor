import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reducer from './reducer';
import './index.css';
import PixelEditor from './PixelEditor';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <PixelEditor />
    </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
