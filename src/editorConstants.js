export const WIDTH = 35;

export const HEIGHT = 28;

export const CELL_WIDTH = 25;

export const LEFT_MOUSE_BUTTON = 1;

export const PREVIEW_WIDTH = WIDTH * 4;

export const PREVIEW_HEIGHT = HEIGHT * 4;

export const CANVAS_WIDTH = WIDTH * CELL_WIDTH;

export const CANVAS_HEIGHT = HEIGHT * CELL_WIDTH;