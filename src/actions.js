export const CHANGE_TOOL = 'changeTool';

export const CELL_DRAWING = 'cellDrawing';

export const CHANGE_COLOR = 'changeColor';

export const CELLS_LIST_DRAWING = 'listDrawing';

export const DRAW_PRIMITIVE = 'drawPrmitive';

export const SET_BOARD_COPY = 'setBoardCopy';

export const CANCEL_DRAW_PRIMITIVE = 'cancelDrawRectangle';

export const CELL_ARRAY_DRAWING = 'cellArrayDrawing';

export const changeTool = (tool) => ({
    type: CHANGE_TOOL,
    tool: tool
});

export const cellDrawing = (pointCoord) => ({
    type: CELL_DRAWING,
    pointCoord
})


export const cellArrayDrawing = (start, end) => ({
    type: CELL_ARRAY_DRAWING,
    start,
    end
})


export const changeColor = (color) => ({
    type: CHANGE_COLOR,
    color: color
})


export const drawPrimitive = (start, end) => ({
    type: DRAW_PRIMITIVE,
    start,
    end
})


export const setBoardCopy = () => ({
    type: SET_BOARD_COPY
})


export const cancelDrawPrimitive = () => ({
    type: CANCEL_DRAW_PRIMITIVE
})
